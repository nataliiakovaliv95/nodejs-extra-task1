const express = require('express');
const router = express.Router();
const User = require('../models/user');
const tokenService = require('../services/tokenService');

router.post('/auth', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  try {
    const user = User.getUserByUsername(username);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    } else if (user.password === password) {
      const token = tokenService.signToken(user);
      return res.status(200).json({
        token,
        user: {
          name: user.name,
          id: user._id
        },
        message: 'You have successfully been logged in'
      });
    } else {
      return res.status(400).json({ message: 'Wrong password' });
    }
  } catch (err) {
    res.status(500).json({
      message: 'Server error'
    });
    console.log(err)
  }
});

module.exports = router;