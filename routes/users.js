const express = require('express');
const router = express.Router();
const User = require('../models/user');
const tokenService = require('../services/tokenService');

router.get('/user/password', tokenService.verifyToken, (req, res) => {
  const user = req.decoded;
  res.status(200).json({
    message: 'Success',
    password: user.password
  });
})

router.get('/', (req, res) => {
  const usersNames = User.getUsersNames();
  res.status(200).json({
    message: 'Success',
    users: usersNames
  });
})

module.exports = router;