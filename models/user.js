const Users = [
  {
    name: 'Nataliia',
    username: 'nata123',
    password: '11111111',
    _id: 1
  },
  {
    name: 'Vita',
    username: 'vita123',
    password: '22222222',
    _id: 2
  },
  {
    name: 'Leo',
    username: 'leo123',
    password: '33333333',
    _id: 3
  },
]

module.exports = Users;

module.exports.getUserByUsername = username => Users.filter(user => user.username === username)[0];

module.exports.getUsersNames = () => Users.map(user => user.name);