const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
const accountRoutes = require('./routes/account');
const usersRoutes = require('./routes/users');

const app = express();

const PORT = 8080;

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), { flags: 'a' });
 
app.use(morgan('combined', { stream: accessLogStream }));
app.use(morgan('combined'))

app.use(cors());
app.use(express.json());

app.use('/account', accountRoutes);
app.use('/users', usersRoutes);

app.listen(PORT, () => {
  console.log('Server has been started');
});