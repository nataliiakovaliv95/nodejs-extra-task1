const jwt = require('jsonwebtoken');
const config = require('../config/secret');

const signToken = user => jwt.sign(user, config.secret, { expiresIn: 3600 * 24 });

const verifyToken = (req, res, next) => {
  try {
    let token = req.headers.authorization;
    if (token) {
      token = token.replace(/^Bearer\s+/, '')
      jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
          return res.json({ message: 'Token is not valid' });
        }
        req.decoded = decoded;
        next();
      });
    } else {
      return res.json({ message: 'Token not provided' });
    }
  }
  catch (err) {
    res.status(500).json({
      message: 'Server error'
    });
    console.log(err)
  }
}

module.exports = {
  signToken,
  verifyToken
};